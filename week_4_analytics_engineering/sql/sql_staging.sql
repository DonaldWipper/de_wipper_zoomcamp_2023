CREATE OR REPLACE EXTERNAL TABLE `zoomcamp2023.dbt.green_tripdata_ext`
              OPTIONS (
    format = 'parquet',
    uris = ['gs://dtc_data_lake_zoomcamp2023/data/green/green_tripdata_2019-*.parquet', 'gs://dtc_data_lake_zoomcamp2023/data/green/green_tripdata_2020-*.parquet']
);


CREATE OR REPLACE EXTERNAL TABLE `zoomcamp2023.dbt.yellow_tripdata_ext`
              OPTIONS (
    format = 'parquet',
    uris = ['gs://dtc_data_lake_zoomcamp2023/data/yellow/yellow_tripdata_2019-*.parquet', 'gs://dtc_data_lake_zoomcamp2023/data/yellow/yellow_tripdata_2020-*.parquet']
);

CREATE OR REPLACE EXTERNAL TABLE `zoomcamp2023.dbt.fhv_tripdata_ext`
              OPTIONS (
    format = 'csv',
    uris = ['gs://dtc_data_lake_zoomcamp2023/data/fhv/fhv_tripdata_2019-*.csv.gz']
);


CREATE OR REPLACE TABLE zoomcamp2023.dbt.green_tripdata AS
SELECT * FROM zoomcamp2023.dbt.green_tripdata_ext;
