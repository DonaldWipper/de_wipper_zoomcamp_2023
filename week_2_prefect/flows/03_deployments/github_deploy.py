from prefect.deployments import Deployment
from parameterized_flow import etl_parent_flow
from prefect.filesystems import GitHub
from prefect.infrastructure import DockerContainer

github_block = GitHub.load("github-block")

deployment = Deployment.build_from_flow(
    flow=etl_parent_flow,
    name="github-example",
    storage=github_block,
    infrastructure=DockerContainer(
        image='prefect-orion:2.7.7',
        image_pull_policy='IF_NOT_PRESENT',
        networks=['prefect']
    ),
    parameters={
        "year": 2020,
        "color": "green",
        "months": [11]
    },
    entrypoint="parameterized_flow.py:etl_parent_flow"
)

if __name__ == "__main__":
    deployment.apply()
