# Prefect DE Zoomcamp week 3

Week 3

### Clustering in big query

Clustering in BigQuery works by physically organizing the data within a table based on the values in one or more
columns. When a table is clustered, related data is stored together on disk, which can help improve query performance by
minimizing the amount of data that needs to be scanned during a query.

**When you create a clustered table in BigQuery, you specify one or more columns to use for clustering. BigQuery then
organizes the data within the table based on the values in those columns. For example, if you create a clustered table
based on a column named category, BigQuery will group all the rows with the same category value together on disk.**

When a query is executed on a clustered table, BigQuery can take advantage of the data organization to optimize the
query performance. For example, if a query includes a filter on the clustered column, BigQuery can use the clustering
information to limit the amount of data that needs to be scanned. Similarly, if a query includes an ORDER BY clause that
matches the clustered column, BigQuery can use the clustering information to optimize the ordering of the results, which
can further improve query performance.

It's important to note that clustering is most effective when it's used in combination with other optimization
techniques, such as partitioning and query tuning. By partitioning the data based on a common column, and clustering the
data within each partition based on a relevant column, you can create a powerful optimization strategy that can help
maximize query performance and reduce costs.

### Partition in big query

In BigQuery, partitioning is a data organization technique that helps to improve query performance by dividing large
tables into smaller, more manageable partitions. Each partition contains a subset of the table's data that shares a
common attribute, such as a date, timestamp, or integer range.

**When a query is executed on a partitioned table, BigQuery uses the partitioning information to limit the amount of data
that needs to be scanned, which can improve query performance and reduce costs. For example, if you have a partitioned
table that is partitioned by date, and you run a query that only requires data from a single date partition, BigQuery
can scan only that partition, rather than scanning the entire table.**

There are two types of partitioning available in BigQuery: ingestion-time partitioning and column-based partitioning.

Ingestion-time partitioning is based on the time when the data was ingested into BigQuery. This is useful for
time-series data, such as log files, sensor data, or transaction data. To use ingestion-time partitioning, you specify a
column that contains a timestamp or date value that represents the ingestion time of each row. BigQuery automatically
creates a new partition for each day's worth of data, based on the time zone specified for the dataset.

Column-based partitioning is based on the values in a specific column of the table. This is useful for tables with a
large number of rows, where queries frequently filter on a specific column. To use column-based partitioning, you
specify a column that contains values that are used to group the data into partitions. For example, you might partition
a table by country, state, or customer ID.

To create a partitioned table in BigQuery, you can use the CREATE TABLE statement with the PARTITION BY clause, like
this:

sql
Copy code
CREATE TABLE mydataset.mytable
PARTITION BY DATE(date_column)
AS SELECT * FROM mydataset.rawdata;
This will create a new table that is partitioned by the date_column column.

Once a table is partitioned, you can run queries against the table as you would with an unpartitioned table, using the
WHERE clause to filter on the partitioning column. BigQuery will automatically use partition pruning to limit the amount
of data that needs to be scanned, which can help improve query performance and reduce costs.
