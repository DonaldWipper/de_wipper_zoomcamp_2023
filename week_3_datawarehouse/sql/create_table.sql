CREATE OR REPLACE EXTERNAL TABLE `zoomcamp2023.fhv.fhv_tripdata`
              OPTIONS (
    format = 'gz',
    uris = ['gs://dtc_data_lake_zoomcamp2023/data/fhv/fhv_tripdata_2019-*.csv.gz']
);


CREATE OR REPLACE TABLE zoomcamp2023.fhv.fhv_tripdata_non_partitoned AS
SELECT * FROM zoomcamp2023.fhv.fhv_tripdata;


select count(distinct affiliated_base_number)
from zoomcamp2023.fhv.fhv_tripdata


select count(distinct affiliated_base_number)
from zoomcamp2023.fhv.fhv_tripdata_non_partitoned


select count(*)
from zoomcamp2023.fhv.fhv_tripdata
where PUlocationID is null
  and DOlocationID is null



CREATE OR REPLACE TABLE zoomcamp2023.fhv.fhv_tripdata_partitoned_clustered
    PARTITION BY DATE(pickup_datetime)
    CLUSTER BY affiliated_base_number AS
SELECT * FROM zoomcamp2023.fhv.fhv_tripdata;


select distinct affiliated_base_number
from zoomcamp2023.fhv.fhv_tripdata_partitoned_clustered
where DATE(pickup_datetime) between '2019-03-01' and '2019-03-31'

select distinct affiliated_base_number
from zoomcamp2023.fhv.fhv_tripdata_non_partitoned
where DATE(pickup_datetime) between '2019-03-01' and '2019-03-31'