import pyspark
from pyspark.sql import types
from pyspark.sql import SparkSession

spark = SparkSession.builder \
    .master("local[*]") \
    .appName('test') \
    .getOrCreate()

df_fhvhv = spark.read.parquet('../fhvhv/*/*')

df_fhvhv.registerTempTable('fhvhv_data')


df_result = spark.sql("""
SELECT 
    count(*) AS cnt
FROM
    fhvhv_data
WHERE pickup_datetime >= '2021-06-15'
      AND pickup_datetime < '2021-06-16'
""").show()
