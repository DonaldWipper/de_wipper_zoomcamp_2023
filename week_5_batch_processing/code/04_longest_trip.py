import pyspark
from pyspark.sql import types
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder \
    .master("local[*]") \
    .appName('test') \
    .getOrCreate()

df_fhvhv = spark.read.parquet('../fhvhv/*/*')

df_fhvhv = df_fhvhv.withColumn('DiffInSeconds',
                               col("dropoff_datetime").cast("long") - col('pickup_datetime').cast("long")).withColumn(
    'DiffInHours', round(col('DiffInSeconds') / 3600))

df_fhvhv.show(100)

df_fhvhv.registerTempTable('fhvhv_data')


df_result = spark.sql("""
SELECT
    MAX(DiffInHours) as _max
FROM
    fhvhv_data

""").show()
