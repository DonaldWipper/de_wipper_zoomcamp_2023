import pyspark
from pyspark.sql import types
from pyspark.sql import SparkSession

spark = SparkSession.builder \
    .master("local[*]") \
    .appName('test') \
    .getOrCreate()

df_fhvhv = spark.read.parquet('../fhvhv/*/*')

df_taxi_zone = spark.read \
    .option("header", "true") \
    .csv('taxi+_zone_lookup.csv')


df_fhvhv.registerTempTable('fhvhv_data')
df_taxi_zone.registerTempTable('taxi_zone')

df_result = spark.sql("""
  select taxi_zone.Zone, count(*) as cty
  from fhvhv_data 
         join taxi_zone 
              on fhvhv_data.PULocationID = taxi_zone.LocationID

group by taxi_zone.Zone
order by count(*) desc
limit 1
""").show()
