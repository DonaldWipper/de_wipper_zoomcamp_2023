-- For the passengers picked up in the Astoria Zone which was the drop up zone that had the largest tip?
-- *
-- Central Park
-- Jamaica
-- South Ozone Park
-- Long Island City/Queens Plaza


select ss."Zone", max(tip_amount)
from green_tripdata s
         join taxi_zone s2
              on s."PULocationID" = s2."LocationID"
         join taxi_zone ss
              on s."DOLocationID" = ss."LocationID"
where s2."Zone" in('Astoria')
group by ss."Zone"
order by max(tip_amount) desc
